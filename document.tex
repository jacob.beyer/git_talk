\input{beamer_template.tex}

\date{24.01.2020}
\title[Git Version Control]{Version Control using Git and Gitlab}

% MATH COMMANDS
\addbibresource{references.bib}


\begin{document}

%------------------------------------------------------------------------------

\maketitle

%------------------------------------------------------------------------------

\begin{frame}{\textbf{Overview}}
  \tableofcontents
\end{frame}

%------------------------------------------------------------------------------

\section{Getting Started}
\subsection{Terminology}
\tframe
    \begin{block}{Repository}<1->
        Represents one \textbf{PROJECT} [Thesis, Code, Presentation, ...] \\
        Always share whole repository \\
    \end{block}

    \begin{block}{Branch}<1->
        Represents one \textbf{TASK} [Chapter, Class, Slide, ...] \\
        Can be shared or local \\
        Branches are independent of each other (until manually combined) \\
    \end{block}

    \begin{block}{Commit}<2>
        Represents the \textbf{INCREMENT OF A BRANCH} [Text done, Fix Bug, ...] \\
        Always belong to one branch \\
        Share the locality of their branch \\
        Commits are \emph{smallest tracked} unit of version control \\
    \end{block}
\end{frame}

%------------------------------------------------------------------------------

\subsection{Local Branch vs Remote Branch}
\tframe
    \centering
    \includegraphics[height=0.8\textheight]{images/remote_vs_local.png}
    \footcite{https://www.git-tower.com/learn/git/ebook/en/command-line/remote-repositories/introduction}
\end{frame}

%------------------------------------------------------------------------------

\subsection{Setting up the Repository}
\tframe
\begin{itemize}
    \item<1> Make an SSH key
        \textcolor{white}{\texttt{\colorbox{black}{\$ssh-keygen}}}
    \item<1> Log into \href{https://git.rwth-aachen.de/}
        {https://git.rwth-aachen.de/} using TIM
    \item<1> Add the key to Gitlab \textit{User-Settings$\rightarrow$ SSH Keys}
    \item<1> Make a repository \textit{Home $\rightarrow$ New Project}
        \begin{itemize}
            \item Initialize with Readme enabled
        \end{itemize}
    \item<1> Navigate to folder where you want the repository to lie on local
        machine
    \item<1> Clone the repository
        \textcolor{white}{\texttt{\colorbox{black}{\$git clone
        git@git.rwth-aachen.de:first.last/project.git}}}
    \item<1> Done
\end{itemize}
\end{frame}

%------------------------------------------------------------------------------

\tikzset{>=latex}
\subsection{Some Basic Local Commands}
\tframe
\only<1>{
\begin{tikzpicture}<1>
    \node[rectangle split, rectangle split parts=2, text width=2.5cm, draw] at (4,-3) (workspace)
    {\textbf{Workspace} \nodepart{second} Work on files normally};
    \node[rectangle split, rectangle split parts=2, text width=2cm, draw] at (8,-3)
    (stage)
    {\textbf{Staging} \nodepart{second} Decide what goes into the next commit};

    \node[color=black] at (6,1) (branch) {\bf HEAD};
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](1.6,0) -- (0.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](3.6,0) -- (2.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](5.6,0) -- (4.4,0);
    \filldraw[color=black,fill=rwthblue50](0,0)circle(0.4)node[color=black]{000};
    \filldraw[color=black,fill=rwthblue50](2,0)circle(0.4)node[color=black]{001};
    \filldraw[color=black,fill=rwthblue50](4,0)circle(0.4)node[color=black]{002};
    \filldraw[color=black,fill=rwthblue50](6,0)circle(0.4)node[color=black]{003};

    \draw[->,color=black, line width=0.06cm](6,-0.4) -- (6,-1) -|
    node[midway, above] {\texttt{git checkout}} (workspace.north);
    \draw[->,color=black, line width=0.06cm](workspace.east) -- node[midway, above]
    {\texttt{git}} node[midway, below] {\texttt{add}} (stage.west);
    \draw[->,color=black, line width=0.06cm] (workspace.south) --++(0,-1.0)
    --++(-2,0) |- node[near start, left] {\texttt{git diff}} (workspace.west);
    \draw[->,color=black, line width=0.06cm] (stage.south) --++(0,-1.0)
    --++(2,0) |- node[near start, right, text width=1.5cm] {\texttt{git status}} (stage.east);
\end{tikzpicture}
}
\only<2>{
\begin{tikzpicture}
    \node[rectangle split, rectangle split parts=2, text width=2.5cm, draw] at (4,-3) (workspace)
    {\textbf{Workspace} \nodepart{second} Work on files normally};
    \node[rectangle split, rectangle split parts=2, text width=2cm, draw] at (8,-3)
    (stage)
    {\textbf{Staging} \nodepart{second} Decide what goes into the next commit};

    \node[color=black] at (8,1) (branch) {\bf HEAD};
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](1.6,0) -- (0.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](3.6,0) -- (2.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](5.6,0) -- (4.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](7.6,0) -- (6.4,0);
    \filldraw[color=black,fill=rwthblue50](0,0)circle(0.4)node[color=black]{000};
    \filldraw[color=black,fill=rwthblue50](2,0)circle(0.4)node[color=black]{001};
    \filldraw[color=black,fill=rwthblue50](4,0)circle(0.4)node[color=black]{002};
    \filldraw[color=black,fill=rwthblue50](6,0)circle(0.4)node[color=black]{003};
    \filldraw[color=black,fill=rwthblue50](8,0)circle(0.4)node[color=black]{004};

    \draw[->,color=black, line width=0.06cm](6,-0.4) -- (6,-1) -|
    node[midway, above] {\texttt{git checkout}} (workspace.north);
    \draw[->,color=black, line width=0.06cm](workspace.east) -- node[midway, above]
    {\texttt{git}} node[midway, below] {\texttt{add}} (stage.west);
    \draw[->,color=black, line width=0.06cm](stage.north) -- node[midway, right,
    text width =1.5cm]
    {\texttt{git commit}} (8,-0.4);
    \draw[->,color=black, line width=0.06cm] (workspace.south) --++(0,-1.0)
    --++(-2,0) |- node[near start, left] {\texttt{git diff}} (workspace.west);
    \draw[->,color=black, line width=0.06cm] (stage.south) --++(0,-1.0)
    --++(2,0) |- node[near start, right, text width=1.5cm] {\texttt{git status}} (stage.east);
\end{tikzpicture}
}
\end{frame}

%------------------------------------------------------------------------------

\tframe
\begin{tikzpicture}<1->
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](1.6,0) -- (0.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](5.6,0) -- (2.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](9.6,0) -- (6.4,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](4,-1) --
    (2.28,-0.28);
    \draw[<-,color = rwthblue50,fill=rwthblue50, line width=0.1cm](8.28,-0.72) --
    (10,0);
    \draw[->,color = rwthblue50,fill=rwthblue50, line width=0.1cm](7.6,-1) --
    (4.4,-1);
    \filldraw[color=black,fill=rwthblue50](0,0)circle(0.4)node[color=black]{000};
    \filldraw[color=black,fill=rwthblue50](2,0)circle(0.4)node[color=black]{001};
    \filldraw[color=black,fill=rwthblue50](6,0)circle(0.4)node[color=black]{002};
    \filldraw[color=black,fill=rwthblue50](10,0)circle(0.4)node[color=black]{003};

    \filldraw[color=black,fill=rwthblue25](4,-1)circle(0.4)node[color=black]{011};
    \filldraw[color=black,fill=rwthblue25](8,-1)circle(0.4)node[color=black]{012};

    \draw[->,color=black, line width=0.06cm](2,-0.4) |- node[midway, below]
    {\texttt{git branch}} (3.6,-1.0);
    \draw[<->,color=black, line width=0.06cm](5,-1) -- node[near start, right]
    {\texttt{git checkout}} (5,0);
    \node[] at (8,1) (meeting){};
    \filldraw[color=black, fill=black] (meeting)circle(0.2)node[color=black,
    above]{\texttt{git merge}};
    \draw[color=black, line width=0.06cm](8,-0.6) -- (meeting);
    \draw[color=black, line width=0.06cm](6,0.4) --++ (0,0.6) -- (meeting);
    \draw[->,color=black, line width=0.06cm](meeting) -| (10,0.4);
\end{tikzpicture}
\uncover<2>{Further local Commands:}
\begin{itemize}
    \item<2> \texttt{git log}: Show the most recent commits
    \item<2> \texttt{git show <branch:filename>}: Show a file from a different
        branch
\end{itemize}
\end{frame}

%------------------------------------------------------------------------------
\subsection{Some Basic Remote Commands}
\tframe
    \centering
    \includegraphics[height=0.8\textheight]{images/remote_vs_local.png}
\end{frame}

%------------------------------------------------------------------------------

\section{Why Version Control?}
\bframe{Advantages of version control}
    \begin{itemize}
        \item<1> \textbf{Very} Thorough Backup
        \item<1> Reverting to any previous state possible
        \item<1> Log of changes allows tracking progress, allows blaming
            mistakes
        \item<1> Keeping and sharing the state across systems
        \item<1> Allows and eases cooperative works
        \begin{itemize}
            \item<1> Controlled Combination of cooperative changes
            \item<1> Accessible to other collaborators
        \end{itemize}
    \end{itemize}
\end{frame}

%------------------------------------------------------------------------------

\section{Improving your Git Experience}
\bframe{General}
    \begin{itemize}
        \item<1> Keep lines short
        \item<1> Separation into multiple files
        \item<1> Commit Commit Commit
        \item<1> Push regularly for backup
        \item<1> Write useful commit messages
    \end{itemize}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Stash}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git stash}}}}
        Allows the "quick-saving" of the current changes without creating
        commits or branches.
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git stash
        apply}}}}
        Allows "quick-loading" of the stashed changes, will be applied
        immediatly to the current state of the repository
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git stash
        list}}}}
        Lists all the recent stashes, with the number n that can be used to
        apply the respective stash
    \end{block}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Tags}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git tag
        <tagname>}}}}
        Creates a tag, which eases returning via \texttt{git checkout <tagname>}
        \\
        Command options:
        \begin{itemize}
            \item<1> \texttt{git tag -a <tag> -m <message>} Creates a Tag with a
                message describing the tag
            \item<1> \texttt{git push origin <tagname>} Pushes the tags to be
                shared with collaborators
        \end{itemize}
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git tag}}}}
        Lists the tags present on the repository
    \end{block}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Global Excludes}
    \begin{block}<1>{\$HOME/.gitconfig}
            [core] \\
            \qquad excludesfile = \$HOME/.config/gitignore
    \end{block}
    \begin{block}<1>{\$HOME/.config/gitignore}
        *.aux *.bbl *.bcf *.blg *.fdb\_latexmk *.fls *.log *.nav *.out
        *.pre *.run.xml *.snm *.toc
    \end{block}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Log Visuals}
    \begin{block}<1->{As much as possible}
        log -\/-graph -\/-decorate -\/-pretty=fuller -\/-abbrev-commit
    \end{block}
    \begin{block}<1->{My configuration \footcite{https://git-scm.com/docs/pretty-formats}}
        log -\/-graph -\/-pretty=format:'\%Cred\%h\%Creset -\%C(yellow)\%d\%Creset \%s
        \\
        \qquad \%Cgreen(\%cr) \%C(bold blue)<\%an>\%Creset' -\/-abbrev-commit
    \end{block}
    \begin{block}<1->{\$HOME/.gitconfig}
    [alias] \\
        \qquad lg = log <options>
    \end{block}
    \begin{block}<2>{File Specific Log}
    \textcolor{white}{\texttt{\colorbox{black}{\$git log -p <filename>}}}
    \end{block}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Rebase vs Merge}
    \vspace{-12pt}
        \begin{minipage}[!t]{0.32\textwidth}
            \vspace{-50pt}
            \includegraphics[width=0.9\textwidth]{images/setup.png}
        \end{minipage} \hfill
        \begin{minipage}[!t]{0.32\textwidth}
            \uncover<2>{\includegraphics[width=0.9\textwidth]{images/merge.png}}
        \end{minipage}
        \begin{minipage}[!t]{0.32\textwidth}
            \uncover<2>{\includegraphics[width=0.9\textwidth]{images/rebase.png}}
        \end{minipage}
    \vspace{-15pt}
    \footcite{https://medium.com/datadriveninvestor/git-rebase-vs-merge-cc5199edd77c}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Random Commands}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git commit
        -amend}}}}
        Change the last commit message
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git blame
        <filename> --since=<time> -n }}}}
        Show the complete changes in one file and who introduced them. Good to go
        bughunting.
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git reset
        --hard }}}}
        Delete all local changes since last commit
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git clean }}}}
        Delete all non-tracked files by git \\
        \texttt{-n}: Show \qquad \texttt{-f}: Remove \qquad
        \texttt{-x}: Ignored files\\
    \end{block}
\end{frame}
%------------------------------------------------------------------------------

\section{Working together}
\bframe{Commit Messages}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git commit -m
        <Message>}}}}
        The "normal" commit message system, title only
    \end{block}
    \begin{block}<1>{ \textcolor{white}{\texttt{\colorbox{black}{\$git commit}}}}
        Opens an editor to write a lengthy commit message, the first line is the
        short title, below a full description can be added. Usefull for major
        changes on important branches.
    \end{block}
\end{frame}

%------------------------------------------------------------------------------

\bframe{Gitlab Groups}
    \centering
    \includegraphics[height=0.6\textwidth]{images/group.jpg}
\end{frame}

%------------------------------------------------------------------------------

\bframe{README, CONTRIBUTING,... }
    \begin{block}<1>{Markdown}
        \emph{Markdown is a lightweight markup language with plain text
        formatting syntax.}
    \end{block}
    \begin{block}<1>{README.md}
        Creating a README.md file in the main folder of the repository will make
        this the readable document at the bottom of the gitlab-main-page of the
        project. \\
    \end{block}
\end{frame}


%------------------------------------------------------------------------------

\section*{Fragen?}
\bframe{}
This is added text on the last slide

\end{frame}

\end{document}
