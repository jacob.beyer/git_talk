# Same in Both
This text was already present in the parent commit of the two branches, meaining
it will not be changed during the merge process.

# Changed in Both
This part was already present in the parent commit, but changes were made in
both branches, resulting in the git mergetool not being able to decide which
version to keep.

In this branch we are adding some line about adding lines to this file.
This line will be marked as the merge-conflict from master/HEAD

As can be seen, only the part which actually changes will be marked during the
merge conflict, the rest of the texts are outside of the merge scope.

# Added in merge-conflicts
This text was added in the merge-conflicts file. It will be appearing in the
resulting file, as it is a change only in one file, with nothing opposing it
from the other branch.
