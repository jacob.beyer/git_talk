
Would you like to talk about Git, our lord and saviour?
Yes? Well you are in luck. This talk will exclusively focus on version control
using git. Some basic instructions and commands will be presented in the start,
but some - in my experience - useful tips and tricks will form the main part.

This will not be a concentrated conversion attempt, but some of the benefits of
using git, even for non-cooperative work, are to be mentioned.

